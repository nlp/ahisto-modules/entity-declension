Entity declension is a command-line tool that is able to extract entities from their source texts and return a list of entities declined to the singular number of the first case. This tool is utilised within the AHISTO project for the indexing of the entities. It can be separated into 4 distinct steps:

1. Extraction
2. Analysis
3. Generation
4. Post-processing

# Extraction

The tool supports extraction from two different types of source texts -- the XML files of abstracts and the vertical files of the AHISTO corpus. Due to the structural differences of these two file types, extraction methods differ slightly.

The extraction step can be carried out via calling the commands `make extract-from-regests` and `make extract-from-corpus`, respectively.

## Abstracts

The extraction tool identifies the smallest spans containing any entity tags (`placeName`, `persName`) -- these spans are then used as contexts for the entities during the analysis step. Examples of the context spans are `issuePlace`, `sibillant`, `testis`, or `p` (paragraph). The entity within the context is replaced with the regularized form of the entity (`reg` attribute within the `entity` tag).

Language of the context is set to Czech (`cs`) by default for the abstracts, due to the fact that the `langUsage` tag does not properly reflect the language of the entities and Czech is the majority language of the entities.

The result of the extraction is a JSON ordered dictionary of contexts where each context has language to be used for analysis, list of entities within the context, and matching lists of entity types and entity author IDs.

## Corpus

The context for entities within the corpus is set to the span of paragraph. The entity withing the context is replaced with the normalized form of the entity (`norm` attribute of the entity tag).

The language for analysis is the majority language detected within the paragraph, with the options being Czech (`cs`), German (`de`), Latin (`la`), and English (`en`).

Due to the amount of entities within the corpus and the ease of matching the extracted entities with their initial position in the source file, the entity types and entity author IDs are retrieved in the post-processing step. Otherwise, the output structure remains the same as for the abstracts.

# Analysis

The extracted text is analysed using the python library stanza, the language of the analysis set to the context language defined in the previous step.

This step can be carried out via calling the `make analysis-regests GID=7` and `make analysis-corpus GID=7`, respectively. This step requires GPU for its computation, the GPU can be selected via setting the `GID` variable for `make`.

The contexts are tokenized and each token within the context is returned with its lemma, XPOS tag, and UPOS tag. As the XPOS tags are language specific, they are further modified to contain a common prefix, denoting the tokens gender, number, and case. If any of this information is not applicable, the character is replaced with `-` instead.

 The entities are tokenized and each entity returns its dependency tree of words within the entity. The ids of the tokens within the dependency tree are modified to match the entity position within the context.

The result of the analysis is a JSON ordered dictionary of tokenized contexts with the added token information where each context has a list of dependency trees of the entities within the context, the language of the context, and the list of original, non-tokenized, entities. The regest entities further return  for each context a list of entity types and entity author IDs, within the dictionary.

# Generation

During the generation step, each token within the entity is evaluated whether on not it is supposed to be lemmatized based on a simple heuristic. 

This step can be carried out via calling the command `make generate-regests` and `make generate-corpus`, respectively.

The root word of the entity is set to be lemmatized and the children of the root word as per the dependency tree are added to the exploration path. Iterating through the current exploration path, if the UPOS tag of the child is `CCONJ` (coordinating conjunction), the child is added to a new exploration path. If the UPOS tag of the child is either `NOUN`, `PROPN`, or `ADJ` (noun, proper noun, or adjective) and the common prefix of the XPOS tag matches with the parent XPOS tag, the child is set to be lemmatized on the output and is added to the new exploration path. After the algorithm has iterated through the current exploration path, it is replaced with the new exploration path. The algorithm finishes when the exploration path is empty.

The tokens that are set to be lemmatized are then replaced with the lemmas returned by the analysis, with the exception of the entities with the context language of Czech. The Czech tokens are lemmatized with the usage of the package `majka`. If `majka` does not return any lemma for the Czech token, the token is replaced with the lemma returned by the initial (`stanza`) analysis.

This step returns a vertical file of the entities and a close window of their context. The file contains a root tag `doc` which contains the attribute `filename`, the name of the source file; tag `context` which contains the attribute `entity`, the original non-tokenized, non-lemmatized form of the entity; and the tag `entity` which contains attributes `type`, the entity type, and `authId`, the entity author id. For the corpus entities the attributes within the entity tag are empty. Each token is written on a separate line, with three following attributes:

1. modified token (lemmatized if applicable)
2. lemma of the token
3. XPOS tag of the token (adjusted for lemmatized tokens)

# Post-processing

The post-processing steps allows the user to account for any inaccuracies of lemmas encountered due to the nature of the input texts. This tool offers three possible methods of output correction:

1. replacement based on the XPOS tag
2. replacement of the full entity
3. replacement of the entity based on regex rules

The results of this tool differ slightly for the regest files and the vertical AHISTO corpus files.

This step can be carried out via calling the command `make modify-regests` and `make modify-corpus`, respectively.

## Abstracts

The outputs of all regest files is concatenated into one common file `out.txt`. Each entity is on a separate line, with four attributes:

1. declined entity
2. entity type
3. source file name
4. entity author ID

## Corpus

The outputs of the corpus files are stored separately within the subfolder `modify` of the corpus output directory. All outputs are further concatenated into one common file `out.txt`. Each entity is on a separate line, with five attributes:

1. declined entity
2. entity type
3. source file name
4. non-normalized original entity
5. (optional) entity author ID, if the entity is present in any of the abstracts

# Usage

Create an XML file `example.xml` containing an entity tag:

```
<p>koruna <persName authId="ah:ee924ca3" reg="krále Vratislava" reg2="krále Vratislava">krále Vratislava</persName></p>
```
Use the `entity-declension` tool to extract, analyse, generate, and post-process the entities within the file by running the following command from the command line:

```
make make-regests GID=7 -j 6
```

The `GID` denotes the id of the GPU to be used during the analysis step, `-j` denotes the amount of parallel jobs run.

The output of the `example.xml` file:

```
král Vratislav  persName  example ah:ee924ca3
```

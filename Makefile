SHELL:=/bin/bash
PYTHON_DIR:=./src
PYTHON3:=/nlp/projekty/ahisto/entity-declension/declension_env/bin/python3

GID=7

CORPUS_SRC:=/nlp/projekty/ahisto/corpus-lemmatized-data
CORPUS_TGT:=/nlp/projekty/ahisto/declension-data/corpus_entities

REBUILD_CORPUS_COUNT:=0
CORPUS_COUNTER=1
CORPUS_ORIG=$(wildcard $(CORPUS_SRC)/*.prim_vert.ner_tags)
CORPUS_EXTRACT=$(CORPUS_ORIG:$(CORPUS_SRC)/%.prim_vert.ner_tags=$(CORPUS_TGT)/extract/%.c_json)
CORPUS_ANALYSIS=$(CORPUS_EXTRACT:$(CORPUS_TGT)/extract/%.c_json=$(CORPUS_TGT)/analyse/%.c_analyse)
CORPUS_GENERATE=$(CORPUS_ANALYSIS:$(CORPUS_TGT)/analyse/%.c_analyse=$(CORPUS_TGT)/generate/%.c_gen)
CORPUS_MODIFY=$(CORPUS_GENERATE:$(CORPUS_TGT)/generate/%.c_gen=$(CORPUS_TGT)/modify/%.c_modify)

REGEST_SRC:=/nlp/projekty/ahisto/public_html/dokumenty
REGEST_TGT:=/nlp/projekty/ahisto/declension-data/regest_entities

space:=' '

REBUILD_REGEST_COUNT=0
REGEST_COUNTER=1
REGEST_ORIG=$(shell find $(REGEST_SRC) -maxdepth 1 -type f -name "*.xml" -not -name "*.orig.xml" | sed -e 's/ /SPACE/g')
REGEST_EXTRACT_MOD=$(patsubst $(REGEST_SRC)/%.xml, $(REGEST_TGT)/extract/%.r_json, $(REGEST_ORIG))
REGEST_EXTRACT=$(subst SPACE,\ , $(REGEST_EXTRACT_MOD))

REGEST_ANALYSIS_MOD=$(patsubst $(REGEST_TGT)/extract/%.r_json, $(REGEST_TGT)/analyse/%.r_analyse, $(REGEST_EXTRACT_MOD))
REGEST_ANALYSIS=$(subst SPACE,\ , $(REGEST_ANALYSIS_MOD))

REGEST_GENERATE_MOD=$(patsubst $(REGEST_TGT)/analyse/%.r_analyse, $(REGEST_TGT)/generate/%.r_gen, $(REGEST_ANALYSIS_MOD))
REGEST_GENERATE=$(subst SPACE,\ , $(REGEST_GENERATE_MOD))


# corpus extract counter
cec:
	@$(eval REBUILD_CORPUS_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(CORPUS_SRC) $(CORPUS_TGT)/extract prim_vert.ner_tags c_json))
	@$(eval CORPUS_COUNTER=1)
# corpus analysis counter
cac:
	@$(eval REBUILD_CORPUS_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(CORPUS_TGT)/extract $(CORPUS_TGT)/analyse c_json c_analyse))
	@$(eval CORPUS_COUNTER=1)
# corpus generate counter
cgc:
	@$(eval REBUILD_CORPUS_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(CORPUS_TGT)/analyse $(CORPUS_TGT)/generate c_analyse c_gen))
	@$(eval CORPUS_COUNTER=1)
# corpus modify counter
cmc:
	@$(eval REBUILD_CORPUS_COUNT=$(words $(CORPUS_ORIG)))
	@$(eval CORPUS_COUNTER=1)

extract-from-corpus: cec $(CORPUS_EXTRACT)
analysis-corpus: cac $(CORPUS_ANALYSIS)
generate-corpus: cgc $(CORPUS_GENERATE)
modify-corpus: cmc
	@rm -rf $(CORPUS_TGT)/modify
	@umask 002; mkdir -p $(CORPUS_TGT)/modify
	@for file in $(CORPUS_GENERATE); do \
		cat $$file >> $(CORPUS_TGT)/generate/combined.c_gen; \
	done
	@umask 002; bash $(PYTHON_DIR)/post_process.sh $(CORPUS_TGT)/generate/combined.c_gen "$(CORPUS_TGT)/modify/combined.c_modify.tmp" replacement/with_tags replacement/full_entity replacement/partial_entity "True"
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/separate_corpus.py $(CORPUS_TGT)/modify/combined.c_modify.tmp $(CORPUS_TGT)/modify
	@rm $(CORPUS_TGT)/modify/combined.c_modify.tmp $(CORPUS_TGT)/generate/combined.c_gen; 
	@for file in $(notdir $(basename $(CORPUS_MODIFY))); do \
		umask 002; $(PYTHON3) $(PYTHON_DIR)/corpus_metadata.py $(CORPUS_TGT)/modify/$$file.c_modify.tmp $(CORPUS_SRC)/$$file.vert.no_entity $(CORPUS_TGT)/modify/$$file.c_modify $(REGEST_TGT)/out.txt; \
		rm $(CORPUS_TGT)/modify/$$file.c_modify.tmp; \
	done	
	@$(MAKE) --no-print-directory combine-corpus-entities REBUILD_CORPUS_COUNT=$(REBUILD_CORPUS_COUNT) 

combine-corpus-entities: $(CORPUS_MODIFY)
	@if [ -f $(CORPUS_TGT)/out.txt ]; then mv $(CORPUS_TGT)/out.txt $(CORPUS_TGT)/out.txt.bak; fi
	@for file in $(CORPUS_MODIFY); do \
		cat $$file >> $(CORPUS_TGT)/out.txt; \
	done

make-corpus:
	@$(MAKE) --no-print-directory extract-from-corpus
	@$(MAKE) --no-print-directory analysis-corpus
	@$(MAKE) --no-print-directory generate-corpus
	@$(MAKE) --no-print-directory modify-corpus

# regest extract counter
rec:
	@$(eval REBUILD_REGEST_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(REGEST_SRC) $(REGEST_TGT)/extract xml r_json))
	@$(eval REGEST_COUNTER=1)
# regest analysis counter
rac:
	$(eval REBUILD_REGEST_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(REGEST_TGT)/extract $(REGEST_TGT)/analyse r_json r_analyse))
	@$(eval REGEST_COUNTER=1)
# regest generate counter
rgc:
	$(eval REBUILD_REGEST_COUNT=$(shell $(PYTHON3) $(PYTHON_DIR)/cmp_timestamps.py $(REGEST_TGT)/analyse $(REGEST_TGT)/generate r_analyse r_gen))
	@$(eval REGEST_COUNTER=1) 

extract-from-regests: rec $(REGEST_EXTRACT)
analysis-regests: rac $(REGEST_ANALYSIS)
generate-regests: rgc $(REGEST_GENERATE)
modify-regests:
	@echo "Correcting entities"
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/combine_verts.py $(REGEST_TGT)/generate $(REGEST_TGT)/entities.vert
	@if [ -f $(REGEST_TGT)/out.txt ]; then mv $(REGEST_TGT)/out.txt $(REGEST_TGT)/out.txt.bak; fi
	@umask 002; bash $(PYTHON_DIR)/post_process.sh $(REGEST_TGT)/entities.vert $(REGEST_TGT)/out.txt replacement/with_tags replacement/full_entity replacement/partial_entity "False"
make-regests:
	@$(MAKE) --no-print-directory extract-from-regests
	@$(MAKE) --no-print-directory analysis-regests
	@$(MAKE) --no-print-directory generate-regests
	@$(MAKE) --no-print-directory modify-regests


help:
	@echo "To (re)generate all corpus files, run the following command:"
	@echo -e "\tmake make-corpus"
	@echo "To (re)generate all corpus files in steps, run the following commands:"
	@echo -e "\t1) make extract-from-corpus"
	@echo -e "\t2) make analysis-corpus"
	@echo -e "\t3) make generate-corpus"
	@echo -e "\t4) make modify-corpus"
	@echo ""
	@echo "To (re)generate all regest files, run the following command:"
	@echo -e "\tmake make-regests"
	@echo "To (re)generate all regest files in steps, run the following commands:"
	@echo -e "\t1) make extract-from-regest"
	@echo -e "\t2) make analysis-regests"
	@echo -e "\t3) make generate-regests"
	@echo -e "\t4) make modify-regests"
	@echo "Analysis requires the usage of gpu which can be set by setting the GID which denotes the id of the gpu, default GID=7."

make-check:
	echo "hi"

$(CORPUS_TGT)/extract/%.c_json: $(CORPUS_SRC)/%.vert.no_entity
	@echo "Extracting corpus file $(CORPUS_COUNTER)/$(REBUILD_CORPUS_COUNT)"
	@umask 002; mkdir -p $(CORPUS_TGT)/extract
	@umask 002; mkdir -p $(CORPUS_TGT)/logs
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/grep_corpus.py $< "$@" $(CORPUS_TGT)/logs/extract.log
	@$(eval CORPUS_COUNTER=$(shell echo $$(($(CORPUS_COUNTER) + 1))))

$(CORPUS_TGT)/analyse/%.c_analyse: $(CORPUS_TGT)/extract/%.c_json
	@echo "Analysing corpus file $(CORPUS_COUNTER)/$(REBUILD_CORPUS_COUNT)"
	@umask 002; mkdir -p $(CORPUS_TGT)/analyse
	@umask 002; mkdir -p $(CORPUS_TGT)/logs
	@umask 002; CUDA_VISIBLE_DEVICES=$(GID) $(PYTHON3) $(PYTHON_DIR)/run_analysis.py $< "$@" $(CORPUS_TGT)/logs/analyse.log
	@$(eval CORPUS_COUNTER=$(shell echo $$(($(CORPUS_COUNTER) + 1))))

$(CORPUS_TGT)/generate/%.c_gen: $(CORPUS_TGT)/analyse/%.c_analyse
	@echo "Generating entities of corpus file $(CORPUS_COUNTER)/$(REBUILD_CORPUS_COUNT)"
	@umask 002; mkdir -p $(CORPUS_TGT)/generate
	@umask 002; mkdir -p $(CORPUS_TGT)/logs
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/run_generate.py $< "$@" $(CORPUS_TGT)/logs/generate.log
	@$(eval CORPUS_COUNTER=$(shell echo $$(($(CORPUS_COUNTER) + 1))))

$(CORPUS_TGT)/modify/%.c_modify: $(CORPUS_TGT)/generate/%.c_gen
	@echo "Correcting entities of corpus file $(CORPUS_COUNTER)/$(REBUILD_CORPUS_COUNT)"
	@umask 002; mkdir -p $(CORPUS_TGT)/modify
	@umask 002; bash $(PYTHON_DIR)/post_process.sh $< "$@.tmp" replacement/with_tags replacement/full_entity replacement/partial_entity "False"
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/corpus_metadata.py $@.tmp $(CORPUS_SRC)/$(notdir $(basename $@)).vert.no_entity $@ $(REGEST_TGT)/out.txt
	@umask 002; rm $@.tmp
	@$(eval CORPUS_COUNTER=$(shell echo $$(($(CORPUS_COUNTER) + 1))))


.SECONDEXPANSION:
$(REGEST_TGT)/extract/%.r_json: $(REGEST_SRC)/$$(subst $$(space),\$$(space),%).xml
	@echo "Extracting regest file $(REGEST_COUNTER)/$(REBUILD_REGEST_COUNT)"
	@umask 002; mkdir -p $(REGEST_TGT)/extract
	@umask 002; mkdir -p $(REGEST_TGT)/logs
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/grep_regest.py "$<" "$@" $(REGEST_TGT)/logs/extract.log
	@$(eval REGEST_COUNTER=$(shell echo $$(($(REGEST_COUNTER) + 1))))

$(REGEST_TGT)/analyse/%.r_analyse: $(REGEST_TGT)/extract/$$(subst $$(space),\$$(space),%).r_json
	@echo "Analysing regest file $(REGEST_COUNTER)/$(REBUILD_REGEST_COUNT)"
	@umask 002; mkdir -p $(REGEST_TGT)/analyse
	@umask 002; mkdir -p $(REGEST_TGT)/logs
	@umask 002; CUDA_VISIBLE_DEVICES=$(GID) $(PYTHON3) $(PYTHON_DIR)/run_analysis.py "$<" "$@" $(REGEST_TGT)/logs/analyse.log
	@$(eval REGEST_COUNTER=$(shell echo $$(($(REGEST_COUNTER) + 1))))

$(REGEST_TGT)/generate/%.r_gen: $(REGEST_TGT)/analyse/$$(subst $$(space),\$$(space),%).r_analyse
	@echo "Generating entities of regest file $(REGEST_COUNTER)/$(REBUILD_REGEST_COUNT)"
	@umask 002; mkdir -p $(REGEST_TGT)/generate
	@umask 002; mkdir -p $(REGEST_TGT)/logs
	@umask 002; $(PYTHON3) $(PYTHON_DIR)/run_generate.py "$<" "$@" $(REGEST_TGT)/logs/generate.log
	@$(eval REGEST_COUNTER=$(shell echo $$(($(REGEST_COUNTER) + 1))))


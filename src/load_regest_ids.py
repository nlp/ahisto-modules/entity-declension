#!/usr/bin/env python3

import sys

def load(filename):
    data = open(filename, 'r', encoding='UTF-8').readlines()
    match_dict = dict()

    for line in data:
        line = line.strip()
        attrs = line.split('\t')
        if len(attrs) > 1:
            match_dict[attrs[0]] = attrs[-1]

    return match_dict

#!/usr/bin/env python3

import re
import sys
import load_regest_ids as lri

entities = open(sys.argv[1], 'r', encoding='UTF-8').readlines()
src_vert = open(sys.argv[2], 'r', encoding='UTF-8').readlines()
output_file = open(sys.argv[3], 'w', encoding='UTF-8')
id_dict = lri.load(sys.argv[4])

filename = sys.argv[1].rsplit('/', 1)[-1].split('.', 1)[0]


types_dict = {'PER': 'persName', 'LOC': 'placeName'}
types = list()

all_tokens = list()
tokens = list()

ent = False

for line in src_vert:
    line = line.strip()
    if line.startswith('<entity'):
        ent = True
        etype = re.findall(r'<entity type=\"([^\"]*?)\"', line)
        show = re.findall(r'display=\"(.*?)\"', line)
        show = True if (show and show[0] == "true") else False
        if not show:
            continue
        if etype:
            types.append(types_dict[etype[0]])
        else:
            types.append('persName')
    elif line.startswith('</entity'):
        if not show:
            continue
        ent = False
        all_tokens.append(tokens)
        tokens = list()
    elif ent:
        if not show:
            continue
        word = line.split('\t', 1)[0]
        tokens.append(word)

for e in entities:
    e = e.strip()
    t = 'persName'
    if types:
        t = types.pop(0)
    toks = ''
    if all_tokens:
        toks = ' '.join(all_tokens.pop(0))
    eid = ''
    if e in id_dict:
        eid = id_dict[e]
    output_file.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(e, t, filename, toks, eid))

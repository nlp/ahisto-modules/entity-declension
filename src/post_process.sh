#!/usr/bin bash

#                        $1            $2      $3        $4          $5             $6
# usage: post_process.sh entities.vert out.txt with_tags full_entity partial_entity "False"

cat $1 > $2.0

while read pattern
  do
    read -a val <<< "$pattern"
    sed -i -e "s/${val[0]}/${val[1]}/g" $2.0
  done < "$3"
 
echo "tags modified"

# $6 is set "True" during modify-corpus, otherwise "False"
python3 ./src/devert.py $2.0 $6 > $2

awk -F '\t' '{print $1}' $2 > $2.1
awk -F '\t' '{printf "%s\t%s\t%s\n", $2, $3, $4}' $2 > $2.23


IFS=$'\t'

# replacement of full entities

while read pattern
  do
    read -a val <<< "$pattern"
    sed -i "s/^${val[0]}$/${val[1]}/g" $2.1
  done < "$4"

echo "full replacements utilised"

paste $2.1 $2.23 > $2
# replacement of parts of entities
while read -r -a pattern
  do
    sed -r -i "s/${pattern[0]}/${pattern[1]}/g" $2
  done < "$5"
 
echo "regex replacements utilised"

sed -i "s/ ,/,/" $2
sed -i "s/ - /-/" $2

rm  $2.0 $2.1 $2.23

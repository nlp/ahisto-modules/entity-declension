#!/usr/bin/env python3

import sys

def separate(filename, out_dir):
    last_file = ""
    data = None
    with open(filename, 'r', encoding='UTF-8') as rf:
        data = rf.readlines()

    entities = list()
    for line in data:
        line = line.strip()
        if not line:
            continue
        if '\t' not in line:
            e = ""
            f = line
        else:
            e, f = line.split('\t')
        if not last_file:
            last_file = f
            entities.append(e)
        elif last_file != f:
            of = open('{0}/{1}.c_modify.tmp'.format(out_dir, last_file), 'w', encoding='UTF-8')
            for ent in entities:
                of.write(ent + '\n')
            of.close()
            
            last_file = f
            entities = [e]
        else:
            entities.append(e)

    if last_file:
        of = open('{0}/{1}.c_modify.tmp'.format(out_dir, last_file), 'w', encoding='UTF-8')
        for ent in entities:
            of.write(ent + '\n')
        of.close()

separate(sys.argv[1], sys.argv[2])

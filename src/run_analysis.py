#!/usr/bin/env python3

import json
import logging
import stanza
from stanza.pipeline.core import DownloadMethod
import sys
import torch
from tqdm import tqdm

log_file = sys.argv[3]
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
handler = logging.FileHandler(log_file, 'a', 'UTF-8')
handler.setFormatter(formatter)
logger.addHandler(handler)


def init_processor(lang):
    if lang == 'cs':
        nlp = stanza.Pipeline(lang='cs', package='pdt', processors='tokenize,mwt,pos,lemma,depparse', download_method=DownloadMethod.REUSE_RESOURCES, verbose=False)
    elif lang == 'en':
        nlp = stanza.Pipeline(lang='en', package='gum', processors='tokenize,pos,lemma,depparse', download_method=DownloadMethod.REUSE_RESOURCES, verbose=False)
    else:
        nlp = stanza.Pipeline(lang=lang, processors='tokenize,pos,lemma,depparse', download_method=DownloadMethod.REUSE_RESOURCES, verbose=False)

    return nlp


def unify_tag(feats, xpos, lang='cs'):
    case = '-'
    gender = '-'
    number = '-'
    tail = xpos if xpos is not None else 'X'
    if feats is None:
        return '----' + tail

    feats_list = feats.split('|')
    feats_dict = dict()
    for f in feats_list:
        name, val = f.split('=')
        feats_dict[name] = val
    
    cases_dict = {"Nom": 1, "Gen": 2, "Dat": 3, "Acc": 4, "Voc": 5, "Loc": 6, "Ins": 7}
    if 'Case' in feats_dict:
        if feats_dict['Case'] not in cases_dict:
            case = str(8)
        else:
            case = str(cases_dict[feats_dict['Case']])

    if 'Gender' in feats_dict:
        gender = feats_dict['Gender'][0]
        if lang == 'cs' and gender == 'M':
            if 'Animacy' in feats_dict and feats_dict['Animacy'] == 'Inan':
                gender = 'I'
    
    if 'Number' in feats_dict:
        number = feats_dict['Number'][0]

    return gender + number + case + '-' + tail


def serialize_context(data, lang):
    words = dict()
    total = 0
    current = 0
    for i in range(len(data.sentences)):
        for w in data.sentences[i].words:
            current += 1
            words[w.id + total] = {'word': w.text, 'lemma': w.lemma, 'tags': unify_tag(w.feats, w.xpos, lang), 'class': w.upos}

        total += current
        current = 0

    return words


def serialize_entity(data, words):
    etok = []
    for i in range(len(data.sentences)):
        for w in data.sentences[i].words:
            etok.append(w.text)
    
    tokens = [words[x]['word'] for x in sorted(words)]
    start = 0
    for i in range(len(tokens)):
        if tokens[i : i + len(etok)] == etok:
            start = i
            break

    end = start + len(etok)

    tree = dict()
    for i in range(len(data.sentences)):
        for w in data.sentences[i].words:
            if (w.head + start) in tree:
                tree[w.head + start].append(w.id + start)
            else:
                tree[w.head + start] = [w.id + start]

    return tree, start, end


def analyse_file(filename, out_filename):
    base_file = filename.rsplit('/', 1)[-1]
    data = None
    with open(filename, 'r', encoding='UTF-8') as rf:
        data = json.load(rf)

    id_split = dict()
    for i in data:
        lang = data[i]['lang']
        if lang in id_split:
            id_split[lang].append(i)
        else:
            id_split[lang] = [i]

    found_langs = set(id_split.keys())
    
    analysed_data = dict()

    logger.info('{0}: {1} languages detected'.format(base_file, len(list(found_langs))))
    
    logger.info('{0}: entity analysis ({1} contexts) started'.format(base_file, len(data)))
    offset = 0
    with tqdm(initial=offset, total=len(data)) as pbar:
        for lang in list(found_langs):    
            processor = init_processor(lang)
        
            for i in id_split[lang]:
                cdict = data[i]
                
                cdata = processor(cdict['context'])
                edata = list()

                for j, e in enumerate(cdict['entities']):
                    edata.append(processor(e))

                analysed_data[int(i)] = {'cdata': cdata, 'edata': edata, 'lang': lang, 'original': cdict['entities'], 'types': None if 'types' not in cdict else cdict['types'], 'ids': None if 'ids' not in cdict else cdict['ids']}
                
                pbar.update(1)
            
    out_data = list()
    for datapoint in sorted(analysed_data):
        words = serialize_context(analysed_data[datapoint]['cdata'], analysed_data[datapoint]['lang'])
        trees = list()
        for e in analysed_data[datapoint]['edata']:
            tree, start, end = serialize_entity(e, words)
            trees.append((tree, start, end))

        out_data.append({'words': words, 'trees': trees, 'lang': analysed_data[datapoint]['lang'], 'original': analysed_data[datapoint]['original'], 'types': analysed_data[datapoint]['types'], 'ids': analysed_data[datapoint]['ids']}) 
        
    logger.info('{0}: entity analysis finished'.format(base_file))
    with open(out_filename, 'w', encoding='UTF-8') as wf:
        json.dump(out_data, wf)

base_file = sys.argv[1].rsplit('/', 1)[-1]
logging.info('{0}: Analysis - start'.format(base_file))
analyse_file(sys.argv[1], sys.argv[2])
logging.info('{0}: Analysis - finished'.format(base_file))
# sys.argv[3] argument used as log_file

#!/usr/bin/env python3

import json
import logging
import os
import re
import sys


def check_brackets(string):
    opened = 0
    for c in string:
        if c == '(':
            opened += 1
        elif c == ')':
            opened -= 1

    return string + max(0, opened) * ')'


def contains_person_or_place(s):
    base_tags = ["persName", "placeName"]
    for tag in base_tags:
        if tag in s:
            return True

    return False


def find_smallest_contexts(f):
    output = list()
    pattern = r"<([^>]*?)(\ [^>]*?)?>(.*?(<persName|<placeName).*?(<\/persName|<\/placeName).*?)<\/\1>"

    cur_finds = re.findall(pattern, f)
    base_tags = ["persName", "placeName"]
    for find in cur_finds:
        if contains_person_or_place(find[0]):
            output.append(f)
            break
        elif find[0] == 'abstract':
            output.append(find[2])
        elif contains_person_or_place(find[2]):
            output.extend(find_smallest_contexts(find[2]))

    if not cur_finds and contains_person_or_place(f):
        output.append(f)

    return output


def clean_up_string(string):
    wo_brackets = re.sub(r'\([^\)]*\)', '', string).replace("[", "").replace("]", "").replace(".", "")
    wo_brackets = wo_brackets.replace(' vnd ', ' und ')
    return wo_brackets


def clean_text(e):
    e = re.sub(r'<[^>]*>', r'', e)
    e = check_brackets(e)
    e = e.lstrip('.').lstrip(' ')
    e = re.sub(r'\([^\)]*\)', '', e)
    e = clean_up_string(e)
    e = re.sub(r'\(|\)', '', e)
    e = re.sub(r'\s+', ' ', e)
    return e.strip()


def check_if_czech_letters(s):
    czech_diacritics = r"[áčďéěíňóřšťúůýž]"
    return any(elem in s.lower() for elem in czech_diacritics)


def get_entities(f):
    contexts = find_smallest_contexts(f)
    if not contexts:
        return dict()

    out_grep = dict()
    extra_contexts = list()

    for context in contexts:
        remove_other_tags = r"<(?!placeName|persName|\/placeName|\/persName)[^>]*?>"
        clean_context = re.sub(remove_other_tags, "", context)
        bracket_pattern = r"\(([^<\)]*?<persName[^>]*>.*?<\/persName>)[^\)]*\)|\([^<\)]*?(<placeName[^>]*>.*?<\/placeName>[^\)]*?)\)"

        hidden_entities = re.findall(bracket_pattern, clean_context)

        for he in hidden_entities:
            text = he[0] if he[0] else he[1]
            lang = 'cs' if check_if_czech_letters(text) else 'la'
            extra_contexts.append((text, lang))

        inside_bracket_pattern = r"<persName authId=\"([^\"]*?)\" reg=\"\s*?\(([^\)\"]*?)\)?\s*?\" [^>]*?>|<placeName authId=\"([^\"]*?)\" reg=\"\s*?\(([^\)\"]*?)\)?\s*?\" [^>]*?>"
    
        bracketed_entities= re.findall(inside_bracket_pattern, context)
        
        for be in bracketed_entities:
                etype, authId, ent = ("persName", be[0], be[1]) if be[1] else ("placeName", be[2], be[3])
                
                if ent:
                    new_context = "<{0} authId=\"{1}\" reg=\"{2}\">{2}</{1}>".format(etype, authId, ent)
                    lang = 'cs' if check_if_czech_letters(ent) else 'la'
                    extra_contexts.append((new_context, lang))
    
    contexts = [(c, 'cs') for c in contexts]
    contexts.extend(extra_contexts)

    for (context, lang) in contexts:
        ebp = [r'(<placeName[^>]*?>)([^\(\)<]+?)[\)\(][^<]*?(<\/placeName>)',
               r'(<placeName[^>]*>)[\(\)][^\)\(<]*?\)?([^<\)\(]*?)(<\/placeName>)',
               r'(<persName[^>]*?>)([^\(\)<]+?)[\)\(][^<]*?(<\/persName>)',
               r'(<persName[^>]*>)[\(\)][^\)\(<]*?\)?([^<\)\(]*?)(<\/persName>)']
        search_context = context
        for pattern in ebp:
            search_context = re.sub(pattern, r'\g<1>\g<2>\g<3>', search_context)
        
        entities = re.findall(r'<persName (authId="[^\"]*") reg="([^\"]*)"[^>]*>|<placeName (authId="[^\"]*") reg="([^\"]*)"[^>]*>', search_context)
        types = re.findall(r'<(persName|placeName)', context)
        
        clean_context = re.sub(r'<[^\>]*>', ' ', context)
        clean_context = clean_up_string(clean_context.strip())
        clean_context = re.sub(r'\s+', ' ', clean_context)
        clean_entities = list()
        
        ent_ids = list()
        for e in entities:
            current_entity = e[1] if e[1] else e[3]
            current_id = e[0] if e[0] else e[2]
            current_id = current_id.split('=')[-1].strip('"')
            current_entity = clean_text(current_entity)
            if current_id or current_entity:
                clean_entities.append(current_entity)
                ent_ids.append(current_id)

        if ent_ids:
            out_grep[max(out_grep) + 1 if out_grep else 0] = {'context': clean_context, 'entities': clean_entities, 'lang': lang, 'types': types, 'ids': ent_ids}

    return out_grep


def extract(filename, out_filename, log_file):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
    handler = logging.FileHandler(log_file, 'a', 'UTF-8')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.info('{0}: Entity extraction started'.format(filename))

    f = open(filename, 'r', encoding='UTF-8').read()
    f = re.sub(r'\s+', ' ', f)

    comment = get_entities(f)
    out_file = open(out_filename, 'w', encoding='UTF-8')
    json.dump(comment, out_file)

    logger.info('{0}: Entity extraction finished'.format(filename))

extract(sys.argv[1], sys.argv[2], sys.argv[3])

#!/usr/bin/env python3

import majka
import sys

def find_form(lemma, majka_tag):
    m = majka.Majka('/nlp/projekty/ajka/lib/majka.lt-w')

    return m.find(lemma + ':' + majka_tag)



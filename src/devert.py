#!/usr/bin/env python3

import ast
import re
import sys


def devert_file(filename, is_corpus="False"):
    data = open(filename, 'r', encoding='UTF-8').readlines()

    is_corpus = ast.literal_eval(is_corpus)
    dtype = None
    ctype = None
    e_start = False
    entity = []
    for line in data:
        if line.strip():
            if line.startswith('<doc'):
                dtype = re.findall(r'filename="([^"]*)"', line)
                if dtype:
                    dtype = dtype[0]

            elif line.startswith('<context'):
                continue
            elif line.startswith('</context'):
                if ctype and not is_corpus:
                    print('{0}\t{1}\t{2}{3}'.format(' '.join(entity), ctype, dtype, '\t{0}'.format(author) if author else ''))
                elif is_corpus:
                    print('{0}\t{1}'.format(' '.join(entity), dtype))
                else:
                    print(' '.join(entity))

                entity = []
            elif line.startswith('<entity'):
                ctype = re.findall(r'type="([^"]*)"', line)
                author = re.findall(r'authId="([^"]*)"', line)
                if ctype:
                    ctype = ctype[0]

                if author:
                    author = author[0]

                e_start = True
            elif line.startswith('</entity'):
                e_start = False
            elif e_start:
                word = line.strip().split('\t', 1)[0]
                entity.append(word)


devert_file(sys.argv[1], sys.argv[2])

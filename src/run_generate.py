#!/usr/bin/env python3

import json
import logging
import sys
from tqdm import tqdm
import pdt_to_majka as p2m
import run_majka


def generate(filename):
    data = None
    with open(filename, 'r', encoding='UTF-8') as rf:
        data = json.load(rf)

    words_out = list()
    for c in tqdm(data):
         
        words = c['words']
        words = {int(k):v for k,v in words.items()}
        lang = c['lang']
        trees = c['trees']
        entities = c['original']
        types = c['types']
        ids = c['ids']
      
        for (tree, start, end) in trees:
            cur_entity = entities[0]
            if tree:
                tree = {int(k):v for k,v in tree.items()}
                top = tree[min(tree.keys())][0]
                if len(words.keys()) > top and words[top]['class'] in ['NOUN', 'ADJ', 'PROPN']:
                    words[top]['lemmatize'] = True
                    path = [top]
                else:
                    path = []

                if len(words.keys()) + start < end:
                    end = len(words.keys()) + start
               
                crawled = set(path)
                
                if 0 <= top <= len(words.keys()) and start == 0 and not cur_entity.startswith(words[top]['word']):
                    path = []
                    tokenized_orig = cur_entity.split(' ')
                    words_out.append('<context entity="{0}">'.format(entities.pop(0)))
                    words_out.append('<entity{0}{1}>'.format(' type="{0}"'.format(types.pop(0) if types else ''), ' authId="{0}"'.format(ids.pop(0) if ids else '')))
                    for t in tokenized_orig:
                        words_out.append('{0}\t{0}\t----'.format(t))
                    words_out.append('</entity>')
                    words_out.append('</context>')
                    continue

                while path:
                    for parent in path:
                        new_parents = []
                        kids = tree[parent] if parent in tree else []
                        for kid in kids:
                            if kid > end:
                                continue

                            if words[parent]['class'] == 'CCONJ':
                                words[kid]['lemmatize'] = True
                                if kid not in crawled:
                                    new_parents.append(kid)

                            elif words[kid]['class'] in ['NOUN', 'ADJ', 'PROPN'] and words[kid]['tags'][:3] == words[parent]['tags'][:3]:
                                words[kid]['lemmatize'] = True
                                if kid not in crawled:
                                    new_parents.append(kid)

                            elif words[kid]['class'] == 'CCONJ':
                                if kid not in crawled:
                                    new_parents.append(kid)

                            crawled.add(kid)
                        path = new_parents

                if len(tree) == 1:
                    for child in tree[min(tree.keys())]:
                        if child in words:
                            words[child]['lemmatize'] = True
            
            words_out.append('<context entity="{0}">'.format(entities.pop(0)))
            closed = True
            for k in words:
                if k == start + 1:
                    words_out.append('<entity{0}{1}>'.format(' type="{0}"'.format(types.pop(0) if types else ''), ' authId="{0}"'.format(ids.pop(0) if ids else '')))
                    closed = False
                if k > max(start - 5, -1) and k <= min(end + 5, len(words)):
                    if lang == 'cs':
                        if 'lemmatize' in words[k] and words[k]['class'] != 'CCONJ':
                            mxpos = words[k]['tags'][4:]
                        
                            mtag = p2m.get_majka_tag(mxpos[:3] + 'S1' + mxpos[5:], words[k]['class'])
                            found = run_majka.find_form(words[k]['lemma'], mtag)
                            if found:
                                words[k]['lemma'] = found[0]['word']
                            
                            words[k]['tags'] = words[k]['tags'][:2] + '1' + words[k]['tags'][3:]

                            if words[k]['word'][0].isupper():
                                words[k]['lemma'] = words[k]['lemma'][0].upper() + words[k]['lemma'][1:]
                    elif 'lemmatize' in words[k] and words[k]['class'] != 'CCONJ' and lang not in ['cs', 'en']:
                        words[k]['tags'] = words[k]['tags'][:2] + '1' + words[k]['tags'][3:]
                        
                    o_lemma = words[k]['lemma'] if words[k]['lemma'] is not None else words[k]['word']
                    o_word = o_lemma if 'lemmatize' in words[k] else words[k]['word']
                    o_tags = words[k]['tags']
                    words_out.append('\t'.join([o_word, o_lemma, o_tags]))
                if k == end:
                    words_out.append('</entity>')
                    closed = True

            if not closed:
                words_out.append('</entity>')

            words_out.append('</context>')

    return words_out


def run(filename, out_filename):
    output = ['<doc filename="{0}">'.format(filename.rsplit('/', 1)[-1].rsplit('.')[0])]
    output.extend(generate(filename))
    output.append('</doc>\n')
    of = open(out_filename, 'w', encoding='UTF-8')
    of.write('\n'.join(output))
    of.close()

log_file = sys.argv[3]
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
handler = logging.FileHandler(log_file, 'a', 'UTF-8')
handler.setFormatter(formatter)
logger.addHandler(handler)

base_file = sys.argv[1].rsplit('/', 1)[-1]
logger.info('{0}: Generation - start'.format(base_file))

run(sys.argv[1], sys.argv[2])

logger.info('{0}: Generation - finished'.format(base_file)) 

#!/usr/bin/env python3

import glob
import os
import sys

def main():
    src_dir = sys.argv[1]
    tgt_dir = sys.argv[2]
    src_extension = sys.argv[3]
    tgt_extension = sys.argv[4]

    src_files = glob.glob('{0}/*[!.orig].{1}'.format(src_dir, src_extension))

    count = 0
   
    if tgt_dir.endswith("modify"):
        #print(len(src_files))
        print(5)
        return

    for sf in src_files:
        tgt_path = '{0}/{1}.{2}'.format(tgt_dir, sf.rsplit('/', 1)[-1].rsplit('.')[0], tgt_extension)
        if os.path.exists(tgt_path):
            if os.stat(sf).st_mtime > os.stat(tgt_path).st_mtime:
                count += 1
        else:
            count += 1

    print(count)


if __name__ == "__main__":
        main()

#!/usr/bin/env python3

import json
import logging
import re
import sys

def check_brackets(string):
    opened = 0
    for c in string:
        if c == '(':
            opened += 1
        elif c == ')':
            opened -= 1

    return string + max(0, opened) * ')'


def clean_up_string(string):
    wo_brackets = string.replace('[', '').replace(']', '').replace('.', '')
    wo_brackets = wo_brackets.replace(' vnd ', ' und ')
    return wo_brackets


def clean_text(e):
    e = re.sub(r'<[^>]*>', '', e)
    e = e.lstrip('.').lstrip(' ')
    e = clean_up_string(e)
    e = re.sub(r'\(|\)', '', e)
    e = re.sub(r'\s+', ' ', e)
    return e.strip()


def find_separations(lines, guard=200):
    new_pars = []
    if len(lines) <= guard:
        return [lines]

    cur_par = []
    entity = False
    for line in lines:
        if line.startswith('</entity'):
            entity = False
        elif len(cur_par) >= guard:
            if not entity:
                new_pars.append(cur_par)
                cur_par = []
        elif line.startswith('<entity'):
            entity = True

        cur_par.append(line)

    if cur_par:
        new_pars.append(cur_par)

    return new_pars


def grep_entities(filename):
    with open(filename, 'r', encoding='UTF-8') as rf:
        data = rf.read()
        txt = re.sub(r'\n', u"\u2063", data)
        
        par_pattern = r'<p>.*?<entity [^>]*>.*?</p>'
        match_pars = re.findall(par_pattern, txt)

        out_grep = dict()
       
        for par in match_pars:
            match_ent = list()
            par_lines = par.split(u"\u2063")
            par_first = []
            langs = {'cs': 0, 'en': 0, 'de': 0, 'la': 0, 'unk': 0}
            
            entity_started = False
            for line in par_lines:
                if line.startswith('<') and line.endswith('>'):
                    if line.startswith('<entity'):
                        entity_started = True
                        entity = re.findall(r'<entity .*? norm=\"(.*?)\" display=\"(.*?)\">', line)
                        display = True if (len(entity) == 1 and entity[0][1] == "true") else False
                        norm = entity[0][0] if entity else ''
                        par_first.append(norm)
                        if display:
                            match_ent.append(norm)
                    elif line.startswith('</entity'):
                        entity_started = False
                    elif not entity_started:
                        par_first.append(line)
                else:
                    line_info = line.split('\t')
                    if not entity_started:
                        par_first.append(line_info[0])
                    langs[line_info[-1]] += 1
            
            par_first = ' '.join(par_first)
            langs['unk'] = -1
            found_lang = max(langs, key=langs.get)
            
            found_ent = list()
            for ent in match_ent:
                found_ent.append(clean_text(ent))
            
            if not found_ent:
                continue
            out_grep[max(out_grep) + 1 if out_grep else 0] = {'context': clean_text(par_first), 'entities': found_ent, 'lang': found_lang}

        return out_grep


def extract(filename, out_filename, log_file):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
    handler = logging.FileHandler(log_file, 'a', 'UTF-8')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    base_file = filename.rsplit('/', 1)[-1]
    logging.info('{0}: Entity extraction started'.format(base_file))

    out_grep = grep_entities(filename)
    out_file = open(out_filename, 'w', encoding='UTF-8')
    json.dump(out_grep, out_file)
    
    logging.info('{0}: Entity extraction finished'.format(base_file))

extract(sys.argv[1], sys.argv[2], sys.argv[3])    

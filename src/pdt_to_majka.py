#!/usr/bin/env python3

def get_majka_tag(orig_tag, cls):
    if cls not in ['NOUN', 'ADJ', 'PROPN']:
        return orig_tag

    mtag = ''
    mtag += 'k' + ('2' if cls == 'ADJ' else '1') # class
    if cls == 'ADJ':
        mtag += 'e' + orig_tag[10] # polarity, A - positive, N - negative

    mtag += 'g' + orig_tag[2] # gender
    mtag += 'n' + orig_tag[3] # plurality
    mtag += 'c' + orig_tag[4] # case
    if cls == 'ADJ':
        mtag += 'd' + orig_tag[9] # degree

    return mtag

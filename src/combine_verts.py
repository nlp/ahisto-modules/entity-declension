#!/usr/bin/env python3

import os
import sys

dirname = sys.argv[1]
files = os.listdir(dirname)
out_file = sys.argv[2]
of = open(out_file, 'w', encoding='UTF-8')

for f in files:
    inputf ='{0}/{1}'.format(dirname, f)
    with open(inputf, 'r', encoding='UTF-8') as rf:
        of.write(rf.read())

of.close()

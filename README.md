# Entity Declension


This is a module showcasing the results of the current entity declension to nominative, sing. of entities from abstracts.

The repository contains the following files:

- [`src/cmp_timestamps.py`]: script returns value of how many files are being processed (used for progress tracking) 
- [`src/grep_corpus.py`|`src/grep_regest.py`]: script extracts named entities from the source file, writing it as a JSON dictionary to the target file. 
	- The script is given two arguments:
		- *src_file*
		- *tgt_file*
	- JSON dictionary has the following structure:
		- *id*: id of the position of the context with a named entity in the source file
		- dictionary of values:
			- *context*: Text of the context containing named entities
			- *entities*: List of named entities in the context
			- *lang*: Main language of the context (for parsing/lemmatization)
			- *types*: List of types of the entities (persName, placeName) for abstracts, null for corpus
			- *ids*: List of author ids of the entites for abstracts, null for corpus
- [`src/run_analysis.py`]: script runs a dependency/lemmatization parser on the context and the entities, returns JSON list containing serialized results from the parser.
	- The script is given two arguments:
		- *src_file*
		- *tgt_file*
	- JSON list contains dictionaries with the following values:
		- *words*: Dictionary containing results of the parser for the context
		- *trees*: A list of hierarchies (dependency tree) of named entities
		- *lang*: Language of the context
		- *original*: List of original versions of the entities, before tokenization
		- *types*: List of types of the entities for abstracts, null for corpus
		- *ids*: List of author ids of the entities for abstracts, null for corpus
- [`src/run_generate.py`]: script generates a vertical file for the entities with a moderate context. 
	- The script is given two arguments:
		- *src_file*
		- *tgt_file*
	- The output file contains three structural tags:
		- *doc* with an attribute *filename* containing the name of the file without directory or extension
		- *context* with an attribute *entity* containing the original entity
		- *entity* with attributes *type* containing the type of the entity and *authId* containing the author id for abstracts, *entity* has no attrbiutes for corpus
	- Each word in the output file has three attributes in three columns:
		- *output_value* of the word with lemmatization applied sometimes
		- *lemma_value* is the lemma of the word
		- *tag* is the XPOS tag of the word, each language uses slightly different tagging methods
- [`src/pdt_to_majka.py`]: script returning majka compact tag for nouns and adjectives for its pdt tag counterpart
- [`src/run_majka.py`]: script returns Czech word for given lemma and majka compact tag
- [`src/combine_verts.py`]: script combines all files in an input directory to one output file
- [`src/devert.py`]: script reverts verticals of entities into one entity per line. This script is meant for the abstract entities
	- Each line has the following four attributes:
		- *entity*
		- *entity_type*
		- *filename*
		- *author_id*
- [`src/load_regest_ids.py`]: script creates a dictionary of entities present in abstracts, with key a declined entity and value being the entity author id
- [`src/corpus_metadata.py`]: script reverts verticals of entities into one entity per line and retrieves additional data from abstracts and the corpus
	- Each line has the following five attributes:
		- *entity*
		- *entity_type*
		- *filename*
		- *original_entity_as_tokens*
		- *author_id* (if applicable, else empty)
- [`replacement/full_entity`]: consists of one entity per line where each line contains the following two columns:
	- version of the entity to be replaced
	- the correct version of the entity
- [`replacement/partial_entity`]: similar to `full_entity` but allows for regex
- [`replacement/with_tags`]: similar to `full_entity` but allows for a replacement of tags
- [`src/post_process.sh`]: script to create the modified output file of the abstract entities. Creates `out.txt` file in the `$(REGEST_TGT)/regest_entities` target directory.
- [`Makefile`]: makefile for running the building steps in the declension of the entities. Makefile currently has the following commands:
	- `help` - shows commands needed to be used to (re)generate the corpus files and the abstract files
	- `make-corpus` - calls the commands `extract-from-corpus`, `analysis-corpus`, `generate-corpus`, `modify-corpus`, in sequence
	- `extract-from-corpus` - extracts `.c_json` files in the target directory from `.vert` files in the source directory
	- `analysis-corpus` - runs parser analysis on the `.c_json` files in the target directory and saves the results in the equivalent `.c_analyse` files in the target directory
	- `generate-corpus` - generates verticals files from the `.c_analyse` files with a moderate context of the entities
	- `modify-corpus` - generates a file with the same structure as indicated in the `src/corpus_metadata.py` output
	- `combine-corpus-entities` - generates a combined file of all entities in the corpus with the same structure as indicated in the `src/corpus_metadata.py` output. This command is a subcommand of `modify-corpus`
	- `make-regests` - calls the commands `extract-from-regests`, `analysis-regests`, `generate-regests`, and `modify-regests`, in sequence
	- `extract-from-regests` - extracts `.r_json` files in the target directory from `.xml` files in the source directory
	- `analysis-regests` - runs parser analysis on the `.r_json` files in the target directory and saves the results in the equivalent `.r_analyse` files in the target directory
	- `generate-regests` - generates vertical files from the `.r_analyse` files with a moderate context of the entities
	- `modify-regests` - generates a file with the same structure as indicated in the `src/devert.py` output


## Replacement Rules

Replacement rules can be found in the directory `replacement`. Here we have three files:

- [`with_tags`]: This file contains rules for the replacement of an entity with tags. First column contains the match to be replaced, second column contains the replacement value. These replacement rules are run as first, while the document is still a vertical file.

	- Example
		- Replacement rule:
			- `Albrechta\([^\t]*\t[^\t]*\t\)F` `Albrecht\1M`
		- Entities to be modified:
			- `Albrechta	Albrechta	FS1-NNFS1-----A----`
		- Output:
			- `Albrecht	Albrechta	MS1-NNMS1-----A----`

- [`full_entity`]: This file contains rules for the full replacement of the entity. First column contains the entity to be replaced, second column contains the replacement entity.

	- Example:
		- Replacement rule:
			- `Polence`	`Polenc`
		- Entities to be modified:
			- `Jan z Polence`
			- `Polence`
		- Output:
			- `Jan z Polence`
			- `Polenc`

- [`partial_entity`]: This file contains rules for a partial replacement of an entity using regex. First column contains the regex to be found, second column contains the expression it is to be replaced with and third column contains the type of the entity it applies to.

	- Example:
		- Replacement rule:
			- `^Albrechta\b`	`Albrecht\b`	`persName`
		- Entities to be modified:
			- `Albrechta V.`
			- `Albrechta`
			- `paní Albrechta`
		- Output:
			- `Albrecht V.`
			- `Albrecht`
			- `paní Albrechta`

